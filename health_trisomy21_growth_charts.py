from trytond.model import fields, ModelSQL, ModelView


class Trisomy21GrowthChartsFCSD(ModelSQL, ModelView):
    'Trisomy 21 GrowthCharts'
    __name__ = "gnuhealth.pediatrics.growth_t21.charts.fcsd"

    indicator = fields.Selection([
        ('l/h-f-a', 'Length/height for age'),
        ('w-f-a', 'Weight for age'),
        ('bmi-f-a', 'Body mass index for age (BMI for age)'),
        ('cp-f-a', 'Cephalic perimeter for age'),
        ], 'Indicator', sort=False, required=True)
    measure = fields.Selection([
        ('p', 'percentiles'),
        ('z', 'z-scores'),
        ], 'Measure')
    sex = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ], 'Sex')
    month = fields.Integer('Month')
    type = fields.Char('Type')
    value = fields.Float('Value')
