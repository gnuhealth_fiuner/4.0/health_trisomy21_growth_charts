from trytond.pool import Pool
from . import health_trisomy21_growth_charts

def register():
    Pool.register(
        health_trisomy21_growth_charts.Trisomy21GrowthChartsFCSD,
        module="health_trisomy21_growth_charts", type_="model"
        )
